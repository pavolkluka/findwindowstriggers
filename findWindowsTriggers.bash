#!/bin/bash
# ########################################################
#  Name:        findWindowsTriggers.bash
#  Version:     1.0
#  Platforms:   Linux
# ########################################################

# SCRIPT VARIABLES
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# ARG VARIABLES
ARG_PATH="$1"

# FILE VARIABLES
FILE_OUTPUT="$DIR_SCRIPT/output_$( date +"%Y%m%d-%H%M" )"

# BIN VARIABLES
BIN_AWK="$( which awk )"
BIN_GREP="$( which grep )"
BIN_ICONV="$( which iconv )"
BIN_MKDIR="$( which mkdir )"
BIN_CD="$( which cd )"
BIN_FIND="$( which find )"
BIN_ECHO="$( which echo )"

# CHECK IF ARGUMENT IS 'ARG_PATH' SET
if [ -z "$ARG_PATH" ]
then
    echo "Please start script with argument, which have path to the Windows directory."
    exit 1
fi

$BIN_FIND $ARG_PATH -type f -exec file -i {} \; | $BIN_AWK -F ':' '/.*xml.*utf-16le$/ {print $1}' | \
while read LINE
do
	VAR_LINE="$LINE"
	VAR_CMD="$($BIN_ICONV -f utf-16 -t utf-8 """$LINE""" | $BIN_GREP -oP '(?<=<Command>).*?(?=</Command>)')"
	VAR_ARG="$($BIN_ICONV -f utf-16 -t utf-8 """$LINE""" | $BIN_GREP -oP '(?<=<Arguments>).*?(?=</Arguments>)')"
	$BIN_ECHO "$VAR_LINE,$VAR_CMD,$VAR_ARG" >> $FILE_OUTPUT
done

