# findWindowsTriggers
Bash script, which looking for windows triggers and save it to CSV file type with comma delimiter.
Output file pattern is output_YEARMONTHDAY-HOURMINUTE.

Fields of CSV output file:

path_to_xml_file,command_of_trigger,arguments_for_command

**Example**

For run this script requires input argument. Argument is path to mounted windows. If you mount Windows to the /mnt/examples, then 

`bash findWindowsTriggers.bash "/mnt/examples"`

**Output**

`/mnt/examples/RunNotepad-20,C:\Windows\System32\NETSTAT.EXE,-ano`

`/mnt/examples/RunNotepad-56,C:\Windows\System32\NETSTAT.EXE,-ano`

`/mnt/examples/RunNotepad-22,C:\Windows\System32\NETSTAT.EXE,-ano`

